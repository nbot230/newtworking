<?php

namespace App\Entity;

use App\Repository\SkillRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: SkillRepository::class)]
class Skill
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['skills', 'skillCategory'])]
    private ?int $id = null;

    #[Groups(['skills', 'skillCategory'])]
    #[ORM\Column(length: 255)]
    private ?string $value = null;

    #[ORM\Column(options: ["default" => 0])]
    private ?int $priority = null;

    #[ORM\ManyToOne(fetch: 'EXTRA_LAZY', inversedBy: 'skills')]
    private ?SkillCategory $category = null;

    #[ORM\ManyToMany(targetEntity: Neighbour::class, mappedBy: 'skills')]
    private Collection $neighbours;

    public function __construct()
    {
        $this->neighbours = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(string $value): static
    {
        $this->value = $value;

        return $this;
    }

    public function getPriority(): ?int
    {
        return $this->priority;
    }

    public function setPriority(int $priority): static
    {
        $this->priority = $priority;

        return $this;
    }

    public function getCategory(): ?SkillCategory
    {
        return $this->category;
    }

    public function setCategory(?SkillCategory $category): static
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return Collection<int, Neighbour>
     */
    public function getNeighbours(): Collection
    {
        return $this->neighbours;
    }

    public function addNeighbour(Neighbour $neighbour): static
    {
        if (!$this->neighbours->contains($neighbour)) {
            $this->neighbours->add($neighbour);
            $neighbour->addSkill($this);
        }

        return $this;
    }

    public function removeNeighbour(Neighbour $neighbour): static
    {
        if ($this->neighbours->removeElement($neighbour)) {
            $neighbour->removeSkill($this);
        }

        return $this;
    }
}
