<?php

namespace App\Entity;

use App\Repository\CityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CityRepository::class)]
class City
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 2555)]
    private ?string $title = null;

    #[ORM\Column(nullable: true)]
    private ?int $priority = null;

    #[ORM\OneToMany(mappedBy: 'city', targetEntity: Neighbour::class)]
    private Collection $neighbours;

    #[ORM\OneToMany(mappedBy: 'city', targetEntity: District::class)]
    private Collection $districts;

    public function __toString(): string
    {
        return $this->getTitle();
    }

    public function __construct()
    {
        $this->neighbours = new ArrayCollection();
        $this->districts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): static
    {
        $this->title = $title;

        return $this;
    }

    public function getPriority(): ?int
    {
        return $this->priority;
    }

    public function setPriority(?int $priority): static
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * @return Collection<int, Neighbour>
     */
    public function getNeighbours(): Collection
    {
        return $this->neighbours;
    }

    public function addNeighbour(Neighbour $neighbour): static
    {
        if (!$this->neighbours->contains($neighbour)) {
            $this->neighbours->add($neighbour);
            $neighbour->setCity($this);
        }

        return $this;
    }

    public function removeNeighbour(Neighbour $neighbour): static
    {
        if ($this->neighbours->removeElement($neighbour)) {
            // set the owning side to null (unless already changed)
            if ($neighbour->getCity() === $this) {
                $neighbour->setCity(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, District>
     */
    public function getDistricts(): Collection
    {
        return $this->districts;
    }

    public function addDistrict(District $district): static
    {
        if (!$this->districts->contains($district)) {
            $this->districts->add($district);
            $district->setCity($this);
        }

        return $this;
    }

    public function removeDistrict(District $district): static
    {
        if ($this->districts->removeElement($district)) {
            // set the owning side to null (unless already changed)
            if ($district->getCity() === $this) {
                $district->setCity(null);
            }
        }

        return $this;
    }
}
