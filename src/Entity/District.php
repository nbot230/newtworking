<?php

namespace App\Entity;

use App\Repository\DistrictRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: DistrictRepository::class)]
class District
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $title = null;

    #[ORM\ManyToOne(inversedBy: 'districts')]
    private ?City $city = null;

    #[ORM\OneToMany(mappedBy: 'district', targetEntity: Neighbour::class)]
    private Collection $neighbours;

    public function __construct()
    {
        $this->neighbours = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): static
    {
        $this->title = $title;

        return $this;
    }

    public function getCity(): ?City
    {
        return $this->city;
    }

    public function setCity(?City $city): static
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return Collection<int, Neighbour>
     */
    public function getNeighbours(): Collection
    {
        return $this->neighbours;
    }

    public function addNeighbour(Neighbour $neighbour): static
    {
        if (!$this->neighbours->contains($neighbour)) {
            $this->neighbours->add($neighbour);
            $neighbour->setDistrict($this);
        }

        return $this;
    }

    public function removeNeighbour(Neighbour $neighbour): static
    {
        if ($this->neighbours->removeElement($neighbour)) {
            // set the owning side to null (unless already changed)
            if ($neighbour->getDistrict() === $this) {
                $neighbour->setDistrict(null);
            }
        }

        return $this;
    }
}
