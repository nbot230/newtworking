<?php

namespace App\Entity;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\ManyToOne;

#[Entity]
class Thread
{
    #[Id]
    #[GeneratedValue]
    #[Column]
    private ?int $id = null;

    #[ManyToOne]
    private Neighbour $neighbour;

    #[Column(nullable: true)]
    private ?string $text;

    #[Column]
    private array $images;

    public function __construct(
        Neighbour $neighbour,
        array $images = [],
        ?string $text = null,
    ) {
        $this->images = $images;
        $this->text = $text;
        $this->neighbour = $neighbour;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getImages(): array
    {
        return $this->images;
    }

    public function getNeighbour(): Neighbour
    {
        return $this->neighbour;
    }

    public function getText(): ?string
    {
        return $this->text;
    }
}
