<?php

namespace App\DTO\Register;

use Symfony\Component\Validator\Constraints as Assert;

class SecondRegisterStepDTO extends AbstractRegisterStepDTO
{
    public function __construct(
       #[Assert\NotBlank]
       #[Assert\Type('integer')]
       public readonly int $city,

        #[Assert\NotBlank]
        #[Assert\Type('integer')]
        public readonly int $district
    ){
    }

    function getStep(): int
    {
        return 2;
    }

    /**
     * @return int
     */
    public function getCity(): int
    {
        return $this->city;
    }

    /**
     * @return int
     */
    public function getDistrict(): int
    {
        return $this->district;
    }
}
