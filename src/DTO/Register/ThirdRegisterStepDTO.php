<?php

namespace App\DTO\Register;

use Symfony\Component\Validator\Constraints as Assert;

class ThirdRegisterStepDTO extends AbstractRegisterStepDTO
{
    public function __construct(
        #[Assert\Collection(
            fields: [
                'skills' => new Assert\Required([
                    new Assert\NotBlank(),
                    new Assert\Type('array'),
                    new Assert\Count(['min' => 1]),
                    new Assert\All([
                        new Assert\Collection([
                            'id' => [
                                new Assert\Type('integer'),
                                new Assert\NotBlank()
                            ]
                        ])
                    ])
                ]),
                'about_me' => new Assert\Required([
                    new Assert\NotBlank(),
                    new Assert\Type('string'),
                    new Assert\Length(['min' => '12'])
                ]),
            ]
        )]
        private readonly array $data
    )
    {
    }

    function getStep(): int
    {
        return 3;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }
}
