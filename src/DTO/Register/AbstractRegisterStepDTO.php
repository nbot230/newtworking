<?php

namespace App\DTO\Register;

abstract class AbstractRegisterStepDTO
{
    abstract function getStep(): int;
}
