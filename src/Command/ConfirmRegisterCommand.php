<?php

namespace App\Command;

use App\Command\Base\AbstractCommand;
use App\DTO\Register\AbstractRegisterStepDTO;
use App\Entity\Neighbour;
use App\UseCase\Base\Handler;

class ConfirmRegisterCommand extends AbstractCommand implements Handler
{
    public function __construct(
        private readonly AbstractRegisterStepDTO $registerStepDTO,
        private readonly Neighbour $neighbour
    ) {
    }

    /**
     * @return Neighbour
     */
    public function getNeighbour(): Neighbour
    {
        return $this->neighbour;
    }

    /**
     * @return AbstractRegisterStepDTO
     */
    public function getRegisterStepDTO(): AbstractRegisterStepDTO
    {
        return $this->registerStepDTO;
    }
}
