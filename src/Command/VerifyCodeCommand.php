<?php

namespace App\Command;

use App\Command\Base\AbstractCommand;
use App\UseCase\Base\Handler;
use Symfony\Component\Validator\Constraints as Assert;

class VerifyCodeCommand extends AbstractCommand implements Handler
{
    public function __construct(
        #[Assert\NotBlank]
        #[Assert\Length(min: 5, max: 5)]
        public readonly string $code,
        #[Assert\NotBlank]
        #[Assert\Length(min: 11, max: 11)]
        public readonly string $phone
    ) {
    }

    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }
}
