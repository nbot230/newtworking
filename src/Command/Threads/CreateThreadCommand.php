<?php

namespace App\Command\Threads;

use App\Command\Base\AbstractCommand;
use App\UseCase\Base\Handler;

class CreateThreadCommand extends AbstractCommand implements Handler
{
    public function __construct(
        public array $images = [],
        public ?string $text = null,
    ) {
    }
}
