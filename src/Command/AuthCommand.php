<?php

namespace App\Command;

use App\Command\Base\AbstractCommand;
use App\UseCase\Base\Handler;
use Symfony\Component\Validator\Constraints as Assert;

class AuthCommand extends AbstractCommand implements Handler
{
    public function __construct(
        #[Assert\NotBlank]
        #[Assert\Length(min: 11, max: 11)]
        public string $phone
    ) {
    }
}
