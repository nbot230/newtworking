<?php

namespace App\Service;

use App\Command\AuthCommand;
use App\Entity\Neighbour;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class AuthService
{
    public function __construct(
        private TokenStorageInterface $tokenStorage,
    ) {
    }

    public function currentUser(): ?Neighbour
    {
        return $this->tokenStorage->getToken()?->getUser();
    }
}
