<?php

namespace App\Service;

use App\Command\VerifyCodeCommand;
use App\Entity\Neighbour;
use App\Repository\NeighbourRepository;

class NeighbourService
{
    public const FINAL_STEP = 3;

    public function __construct(private readonly NeighbourRepository $neighbourRepository)
    {
    }

    public function registerNew(VerifyCodeCommand $authDTO): Neighbour
    {
        $neighbour = new Neighbour();
        $neighbour->setPhone($authDTO->phone);
        $neighbour->setRegisterStep(2);
        $neighbour->setRoles([$neighbour::USER]);

        $this->neighbourRepository->save($neighbour);

        return $neighbour;
    }
}
