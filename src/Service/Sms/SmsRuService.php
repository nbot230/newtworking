<?php

namespace App\Service\Sms;

use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class SmsRuService implements SendsSms
{
    public function __construct(
        private readonly string $secret,
        private readonly HttpClientInterface $smsruClient
    )
    {
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function send(string $message, Smsable $to): void
    {
        $this->smsruClient->request('GET', '/sms/send',[
            'query' => [
                'api_id' => $this->secret,
                'to' => $to->getPhone(),
                'msg' => $message,
                'json' => 1
            ]
        ]);
    }
}
