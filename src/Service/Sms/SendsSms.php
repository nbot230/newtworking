<?php

namespace App\Service\Sms;

interface SendsSms
{
    public function send(string $message, Smsable $to);
}
