<?php

namespace App\Service\Sms;

interface Smsable
{
    public function getPhone(): ?string;
}
