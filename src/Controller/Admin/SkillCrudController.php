<?php

namespace App\Controller\Admin;

use App\Entity\Skill;
use App\Repository\SkillRepository;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class SkillCrudController extends AbstractCrudController
{
    public function __construct(
        private readonly SkillRepository $skillRepository,
    ) {
    }

    public static function getEntityFqcn(): string
    {
        return Skill::class;
    }

    #[Route('/skills', name: 'skills', methods: ['GET'])]
    public function getSkills(): JsonResponse
    {
        $skills = $this->skillRepository
            ->createQueryBuilder('s')
            ->orderBy('s.priority', 'DESC')
            ->getQuery()
            ->getResult();

        return $this->json($skills, Response::HTTP_OK, [], ['groups' => ['skills', 'skillCategory']]);
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('value', 'Название'),
            NumberField::new('priority', 'Приоритет отображения'),
            AssociationField::new('category', 'Категория')->setCrudController(SkillCategoryCrudController::class),
        ];
    }
}
