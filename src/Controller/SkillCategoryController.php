<?php

namespace App\Controller;

use App\Repository\SkillCategoryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SkillCategoryController extends AbstractController
{
    public function __construct(
        private readonly SkillCategoryRepository $skillCategoryRepository
    ) {
    }

    #[Route('/skills/categories', name: 'skill_categories', methods: ['GET'])]
    public function getSkillsByCategories(): JsonResponse
    {
        $skills = $this->skillCategoryRepository
            ->createQueryBuilder('s')
            ->getQuery()
            ->getResult();

        return $this->json($skills, Response::HTTP_OK, [], ['groups' => ['skillCategory']]);
    }
}
