<?php

namespace App\Controller;

use App\Command\AuthCommand;
use App\Command\Threads\CreateThreadCommand;
use App\Entity\Neighbour;
use App\UseCase\Base\HandlerBus;
use App\UseCase\Threads\CreateThreadCommandHandler;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;

#[Route("/thread")]
class ThreadController extends AbstractController
{
    public function __construct(
        private readonly HandlerBus $bus
    ) {
    }

    #[Route('', methods: ['POST'])]
    public function createThread(
        #[MapRequestPayload(acceptFormat: 'json')] CreateThreadCommand $command,
        #[CurrentUser] Neighbour $user,
        CreateThreadCommandHandler $handler
    ): JsonResponse {
        $this->bus->dispatch($command);
        return $this->json([
            'id' => $command->result['id']
        ]);
    }
}
