<?php

namespace App\Controller;

use App\Command\AuthCommand;
use App\Command\ConfirmRegisterCommand;
use App\Command\RefreshTokenCommand;
use App\Command\VerifyCodeCommand;
use App\DTO\Register\SecondRegisterStepDTO;
use App\DTO\Register\ThirdRegisterStepDTO;
use App\Entity\Neighbour;
use App\UseCase\Base\HandlerBus;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Attribute\MapQueryString;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;

class NeighbourController extends AbstractController
{
    public function __construct(
        private readonly HandlerBus $bus
    ) {
    }

    #[Route('/auth', name: 'auth_user', methods: ['POST'])]
    public function auth(
        #[MapRequestPayload(acceptFormat: 'json')] AuthCommand $authDTO
    ): JsonResponse {
        $authDTO->result = [];

        $this->bus->dispatch($authDTO);

        return $this->json($authDTO->result);
    }

    #[Route('verify/code', name: 'verify_code', methods: ['POST'])]
    public function verifyCode(
        #[MapQueryString] VerifyCodeCommand $verifyCodeCommand
    ): JsonResponse {
        $this->bus->dispatch($verifyCodeCommand);

        return $this->json($verifyCodeCommand->result);
    }

    #[Route('refresh/token/{token}', name: 'refresh_token', methods: ['POST'])]
    public function refreshToken(string $token): JsonResponse
    {
        $this->bus->dispatch($command = new RefreshTokenCommand(refreshToken: $token));

        return $this->json($command->result);
    }

    #[Route('/confirm/step/2', name: 'confirm_reg_second', methods: ['POST'])]
    public function confirmSecondStep(
        #[CurrentUser] Neighbour $neighbour,
        #[MapRequestPayload] SecondRegisterStepDTO $confirmSecondStepDTO
    ): JsonResponse {
        if ($neighbour->getRegisterStep() !== $confirmSecondStepDTO->getStep()) {
            throw new ($this->createAccessDeniedException('Not the right register step'));
        }

        $this->bus->dispatch($command = new ConfirmRegisterCommand(
            registerStepDTO: $confirmSecondStepDTO,
            neighbour: $neighbour
        ));

        return $this->json($command->result);
    }

    #[Route('/confirm/step/3', name: 'confirm_reg_third', methods: ['POST'])]
    public function confirmThirdStep(
        #[CurrentUser] Neighbour $neighbour,
        #[MapRequestPayload] ThirdRegisterStepDTO $confirmThirdStepDTO
    ): JsonResponse {
        if ($neighbour->alreadyRegistered()) {
            throw new ($this->createAccessDeniedException('Not the right register step'));
        }

        $this->bus->dispatch($command = new ConfirmRegisterCommand(registerStepDTO: $confirmThirdStepDTO, neighbour: $neighbour));

        return $this->json($command->result);
    }
}
