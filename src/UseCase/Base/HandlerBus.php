<?php

namespace App\UseCase\Base;

interface HandlerBus
{
    public function dispatch(Handler $handler): void;
}
