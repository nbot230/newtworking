<?php

namespace App\UseCase;

use App\Command\ConfirmRegisterCommand;
use App\DTO\Register\SecondRegisterStepDTO;
use App\DTO\Register\ThirdRegisterStepDTO;
use App\Entity\District;
use App\Repository\CityRepository;
use App\Repository\NeighbourRepository;
use App\Repository\SkillRepository;
use App\UseCase\Base\CommandHandler;

class ConfirmRegisterStepCommandHandler implements CommandHandler
{
    public function __construct(
        private readonly NeighbourRepository $neighbourRepository,
        private readonly SkillRepository $skillRepository,
        private readonly CityRepository $cityRepository
    ) {
    }

    public function __invoke(ConfirmRegisterCommand $confirmRegisterCommand): void
    {
        match ($confirmRegisterCommand->getNeighbour()->getRegisterStep()) {
            2 => (function () use ($confirmRegisterCommand) {
                $neighbour = $confirmRegisterCommand->getNeighbour();

                /** @var SecondRegisterStepDTO $confirmRegisterDto */
                $confirmRegisterDto = $confirmRegisterCommand->getRegisterStepDTO();

                $city = $this->cityRepository->find($confirmRegisterDto->getCity());

                $neighbour->setCity($city);

                $districtsOfCity = $city->getDistricts();

                $findCityDistrict = fn(int $i, District $district) => $district->getId() === $confirmRegisterDto->getDistrict();

                $neighbour->setDistrict($districtsOfCity->findFirst($findCityDistrict));

                $neighbour->setRegisterStep(3);

                $this->neighbourRepository->save($neighbour, true);

                $confirmRegisterCommand->result = [
                    'success' => true,
                    'step' => $neighbour->getRegisterStep()
                ];
            })(),
            3 => (function () use ($confirmRegisterCommand) {
                $neighbour = $confirmRegisterCommand->getNeighbour();

                /** @var ThirdRegisterStepDTO $confirmRegisterDto */
                $confirmRegisterDto = $confirmRegisterCommand->getRegisterStepDTO();
                $ids = array_map(fn($skill) => $skill['id'], $confirmRegisterDto->getData()['skills']);
                $skills = $this->skillRepository->findBy(['id' => array_values($ids)]);

                foreach ($skills as $skill) {
                    $neighbour->addSkill($skill);
                }

                $neighbour->setAbout($confirmRegisterDto->getData()['about_me']);

                $neighbour->completeRegister();

                $this->neighbourRepository->save($neighbour, true);

                $confirmRegisterCommand->result = [
                    'success' => true,
                    'step' => $neighbour->getRegisterStep(),
                    'complete' => true
                ];
            })()
        };
    }
}
