<?php

namespace App\UseCase\Threads;

use App\Command\Threads\CreateThreadCommand;
use App\Entity\Neighbour;
use App\Entity\Thread;
use App\Repository\ThreadRepository;
use App\Service\AuthService;
use App\UseCase\Base\CommandHandler;

class CreateThreadCommandHandler implements CommandHandler
{
    public function __construct(
        private AuthService $authService,
        private ThreadRepository $repository,
    ) {
    }

    public function __invoke(CreateThreadCommand $command): void
    {
        $neighbour = $this->authService->currentUser();
        $thread = new Thread($neighbour, [], $command->text);
        $this->repository->save($thread);
        $command->result['id'] = $thread->getId();
    }
}
