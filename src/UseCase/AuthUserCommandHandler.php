<?php

namespace App\UseCase;

use App\Command\AuthCommand;
use App\Entity\Neighbour;
use App\Service\Sms\SendsSms;
use App\UseCase\Base\CommandHandler;
use Psr\Cache\InvalidArgumentException;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;

class AuthUserCommandHandler implements CommandHandler
{
    public function __construct(
        private readonly CacheInterface $cache,
        private readonly SendsSms $smsRuService,
    ) {
    }

    /**
     * @throws InvalidArgumentException
     */
    public function __invoke(AuthCommand $authDTO): void
    {
        $code = (string)mt_rand(10000, 99999);

        $this->cache->get($code, function(ItemInterface $i) use ($authDTO) {
            $i->expiresAfter(18000);

            return $authDTO->phone;
        });

        $this->smsRuService->send($code, (new Neighbour())->setPhone('89087098056'));

        $authDTO->result['success'] = true;
    }
}
