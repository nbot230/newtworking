<?php

namespace App\UseCase;

use App\Command\VerifyCodeCommand;
use App\Repository\NeighbourRepository;
use App\Security\AuthTokenRepository;
use App\Service\NeighbourService;
use App\UseCase\Base\CommandHandler;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;

class VerifyCodeCommandHandler implements CommandHandler
{
    public function __construct(
        private readonly CacheInterface $cache,
        private readonly NeighbourRepository $repository,
        private readonly NeighbourService $neighbourService,
        private readonly AuthTokenRepository $authTokenRepository
    ) {
    }

    /**
     * @throws InvalidArgumentException
     */
    public function __invoke(VerifyCodeCommand $verifyCodeCommand): void
    {
        if (!$this->cache->hasItem($verifyCodeCommand->code)) {
            throw new UnauthorizedHttpException('', 'Wrong code provided');
        }

        $number = $this->cache->get($verifyCodeCommand->code, function (ItemInterface $i) {
            return $i->get();
        });

        if ($number !== $verifyCodeCommand->getPhone()) {
            throw new UnauthorizedHttpException(challenge: '', message: sprintf('Wrong phone provided:  %s', $verifyCodeCommand->getPhone()));
        }

        $neighbour = $this->repository->findOneBy([
            'phone' => $verifyCodeCommand->getPhone()
        ]);

        if ($neighbour === null) {
            $neighbour = $this->neighbourService->registerNew($verifyCodeCommand);
        }

        $this->authTokenRepository->store($authToken = uniqid('', true), $neighbour);

        $neighbour->setRefreshToken($refreshToken = uniqid('', true));

        $this->repository->save($neighbour, true);

        $result = [
            'refreshToken' => $refreshToken,
            'accessToken' => $authToken,
            'registerStep' => $neighbour->getRegisterStep(),
        ];

        $verifyCodeCommand->result = $result;
    }
}
