<?php

namespace App\UseCase;

use App\Command\RefreshTokenCommand;
use App\Repository\NeighbourRepository;
use App\Security\AuthTokenRepository;
use App\UseCase\Base\CommandHandler;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class RefreshTokenCommandHandler implements CommandHandler
{
    public function __construct(
        private readonly NeighbourRepository $neighbourRepository,
        private readonly AuthTokenRepository $authTokenRepository,
        private readonly EntityManagerInterface $em
    ) {
    }

    public function __invoke(RefreshTokenCommand $refreshTokenCommand): void
    {
        $neighbour = $this->neighbourRepository->findOneBy([
            'refreshToken' => $refreshTokenCommand->refreshToken
        ]);

        if ($neighbour === null) {
            throw new UnauthorizedHttpException('', 'Invalid credentials');
        }

        $neighbour->setRefreshToken($refreshToken = uniqid('', true));

        $this->em->flush();

        $this->authTokenRepository->store($accessToken = uniqid('', true), $neighbour);

        $refreshTokenCommand->result = [
            'accessToken' => $accessToken,
            'refreshToken' => $refreshToken,
        ];
    }
}
