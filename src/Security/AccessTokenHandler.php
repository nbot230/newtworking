<?php

namespace App\Security;

use Psr\Cache\InvalidArgumentException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Http\AccessToken\AccessTokenHandlerInterface;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;

class AccessTokenHandler implements AccessTokenHandlerInterface
{
    public function __construct(
        private readonly AuthTokenRepository $repository
    ) {
    }

    /**
     * @inheritDoc
     * @throws InvalidArgumentException
     */
    public function getUserBadgeFrom(#[\SensitiveParameter] string $accessToken): UserBadge
    {
        $userId = $this->repository->findOneByValue($accessToken);
        if (null === $userId) {
            throw new BadCredentialsException('Invalid credentials.');
        }

        return new UserBadge($userId);
    }
}
