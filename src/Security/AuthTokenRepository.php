<?php

namespace App\Security;

use App\Entity\Neighbour;
use Psr\Cache\InvalidArgumentException;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;

class AuthTokenRepository
{
    public function __construct(
        private readonly CacheInterface $cache
    ) {
    }

    public function store(string $accessToken, Neighbour $neighbour): void
    {
        $this->cache->get($accessToken, function (ItemInterface $item) use ($neighbour) {
            $item->expiresAfter(3600 * 24 * 15);

            return $neighbour->getUserIdentifier();
        });
    }

    /**
     * @throws InvalidArgumentException
     */
    public function findOneByValue(string $accessToken): string | int | null
    {
        return $this->cache->get($accessToken, function (ItemInterface $i) {
            return $i->get();
        });
    }
}
