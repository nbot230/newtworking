<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230723183457 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE thread_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE thread (id INT NOT NULL, neighbour_id INT DEFAULT NULL, text VARCHAR(255) DEFAULT NULL, images JSON NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_31204C83144C013 ON thread (neighbour_id)');
        $this->addSql('ALTER TABLE thread ADD CONSTRAINT FK_31204C83144C013 FOREIGN KEY (neighbour_id) REFERENCES neighbour (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE thread_id_seq CASCADE');
        $this->addSql('ALTER TABLE thread DROP CONSTRAINT FK_31204C83144C013');
        $this->addSql('DROP TABLE thread');
    }
}
