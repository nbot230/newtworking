<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230711193359 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE district_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE district (id INT NOT NULL, city_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_31C154878BAC62AF ON district (city_id)');
        $this->addSql('CREATE TABLE neighbour_skill (neighbour_id INT NOT NULL, skill_id INT NOT NULL, PRIMARY KEY(neighbour_id, skill_id))');
        $this->addSql('CREATE INDEX IDX_E313F0FF144C013 ON neighbour_skill (neighbour_id)');
        $this->addSql('CREATE INDEX IDX_E313F0FF5585C142 ON neighbour_skill (skill_id)');
        $this->addSql('ALTER TABLE district ADD CONSTRAINT FK_31C154878BAC62AF FOREIGN KEY (city_id) REFERENCES city (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE neighbour_skill ADD CONSTRAINT FK_E313F0FF144C013 FOREIGN KEY (neighbour_id) REFERENCES neighbour (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE neighbour_skill ADD CONSTRAINT FK_E313F0FF5585C142 FOREIGN KEY (skill_id) REFERENCES skill (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE neighbour ADD city_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE neighbour ADD district_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE neighbour ADD about TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE neighbour ADD CONSTRAINT FK_76D9C4298BAC62AF FOREIGN KEY (city_id) REFERENCES city (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE neighbour ADD CONSTRAINT FK_76D9C429B08FA272 FOREIGN KEY (district_id) REFERENCES district (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_76D9C4298BAC62AF ON neighbour (city_id)');
        $this->addSql('CREATE INDEX IDX_76D9C429B08FA272 ON neighbour (district_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE neighbour DROP CONSTRAINT FK_76D9C429B08FA272');
        $this->addSql('DROP SEQUENCE district_id_seq CASCADE');
        $this->addSql('ALTER TABLE district DROP CONSTRAINT FK_31C154878BAC62AF');
        $this->addSql('ALTER TABLE neighbour_skill DROP CONSTRAINT FK_E313F0FF144C013');
        $this->addSql('ALTER TABLE neighbour_skill DROP CONSTRAINT FK_E313F0FF5585C142');
        $this->addSql('DROP TABLE district');
        $this->addSql('DROP TABLE neighbour_skill');
        $this->addSql('ALTER TABLE neighbour DROP CONSTRAINT FK_76D9C4298BAC62AF');
        $this->addSql('DROP INDEX IDX_76D9C4298BAC62AF');
        $this->addSql('DROP INDEX IDX_76D9C429B08FA272');
        $this->addSql('ALTER TABLE neighbour DROP city_id');
        $this->addSql('ALTER TABLE neighbour DROP district_id');
        $this->addSql('ALTER TABLE neighbour DROP about');
    }
}
